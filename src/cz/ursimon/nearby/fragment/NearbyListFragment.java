package cz.ursimon.nearby.fragment;

import com.android.volley.toolbox.NetworkImageView;

import cz.ursimon.nearby.R;
import cz.ursimon.nearby.app.MyVolley;
import cz.ursimon.nearby.data.Constants;
import cz.ursimon.nearby.provider.ContentProviderColumnsHelper;
import cz.ursimon.nearby.provider.PlacesContentProvider;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class NearbyListFragment 
extends ListFragment 
implements LoaderManager.LoaderCallbacks<Cursor>,
OnItemClickListener
{
	
	public final static String TAG_FRAGMENT_LIST = "restaurants-fragment-list";
	private final static int LIST_ID = 123;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getLoaderManager().restartLoader(LIST_ID, null, this);	
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle b) {
                return new CursorLoader(
                		this.getActivity(), 
                		PlacesContentProvider.GEOPOINT_URI, 
                		ContentProviderColumnsHelper.columnsGeopoint, 
                		null, 
                		null,
                		PlacesContentProvider.ROW_ID
                		);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor c) {
		if (this.getListAdapter()==null) {
			SimpleCursorAdapter mAdapter = 
					new SimpleCursorAdapter(
							getActivity(), 
							R.layout.simple_list_item_2,
							c,
							new String[] {PlacesContentProvider.GEOPOINT_NAME,PlacesContentProvider.GEOPOINT_VICINITY, PlacesContentProvider.GEOPOINT_THUMB},
							new int[] {android.R.id.text1,android.R.id.text2,R.id.item_list_image},
							0
							);
			
			mAdapter.setViewBinder(new ViewBinder() {

				public boolean setViewValue(View aView, Cursor aCursor, int aColumnIndex) {
					if (aView instanceof NetworkImageView) {
						NetworkImageView image = (NetworkImageView) aView;
			        	String url = aCursor.getString(aColumnIndex);			        	
			        	//Log.v(TAG,"adapter viewbinder image:"+url);
			        	image.setErrorImageResId(R.drawable.ic_launcher);
			        	image.setDefaultImageResId(R.drawable.ic_launcher);
			        	String myUrl = "http://badurl-for-sure.com";
			        	if (url!=null) {
			        		if (
			        			(url.length()>0) &&
			        			(!(url.toLowerCase().contains(".gif"))) // does not contain GIF!
			        		   )
			        			{
			        			//Log.v(TAG,"loading image thumb:"+url);
			        			myUrl = url;
			        			myUrl = String.format(Constants.PLACES_IMAGE_URL_TEMPLATE, 
								        					myUrl, 
								        					getResources().getString(R.string.api_key_maps_web));
			        		}
			        	}
			        	image.setImageUrl(myUrl, MyVolley.getImageLoader());
			        	return true;
					} else return false;
				}
			});
			
			setListAdapter(mAdapter);
			getListView().setOnItemClickListener(this);
		} else { // just swapping cursor
			((SimpleCursorAdapter)this.getListAdapter()).changeCursor(c);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> l) {
		// TODO Auto-generated method stub		
	}
	
	private void launchGoogleMaps(String name, String vicinity) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
			Uri.parse("http://maps.google.com/maps?q="+name+","+vicinity));
			intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
			startActivity(intent);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
	 Cursor item = (Cursor)getListAdapter().getItem(position);	
	 launchGoogleMaps(
		 item.getString(item.getColumnIndexOrThrow(PlacesContentProvider.GEOPOINT_NAME)),
		 item.getString(item.getColumnIndexOrThrow(PlacesContentProvider.GEOPOINT_VICINITY))
	 );
	}

}
