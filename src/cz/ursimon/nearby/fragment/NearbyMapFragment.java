package cz.ursimon.nearby.fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import cz.ursimon.nearby.MainActivity;
import cz.ursimon.nearby.R;
import cz.ursimon.nearby.provider.ContentProviderColumnsHelper;
import cz.ursimon.nearby.provider.PlacesContentProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class NearbyMapFragment
extends Fragment 
implements LoaderManager.LoaderCallbacks<Cursor>, 
OnMapClickListener, 
OnInfoWindowClickListener

{

	private final String TAG = getClass().getSimpleName().toString();
	public final static String TAG_FRAGMENT_MAP = "restaurants-fragment-map";

	private GoogleMap mMap;
	private SupportMapFragment mapFragment;
	private final static int MAP_ID = 321;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.v(TAG, "onCreateView");
		View v = inflater.inflate(R.layout.fragment_map, container, false);
		setUpMapIfNeeded();
		return v;
	}
	
	/**
	 * add pin to map
	 * @param latLng
	 */
	private void addPin(double lat, double lon, String name, String snippet) {

		LatLng latLng = new LatLng(lat, lon);		
		Log.v(TAG,"adding pin: "+lat+" "+lon+" "+name);
		if(mMap != null) {
			MarkerOptions pin = new MarkerOptions();
			pin.position(latLng);
			pin.title(name);
			pin.snippet(snippet);
			mMap.addMarker(pin);
//			mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
		}
	}
	
	/**
	 * Setup map if needed
	 */
	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			if (mapFragment == null) {

				mMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
				if (mMap != null) {

					//mMap = mapFragment.getMap();
					Log.d(TAG, "map != null");
					UiSettings ui = mMap.getUiSettings();
					ui.setCompassEnabled(true);
					ui.setZoomControlsEnabled(true);
					ui.setMyLocationButtonEnabled(true);
					mMap.setMyLocationEnabled(true);
					//mMap.setTrafficEnabled(true);
					mMap.setIndoorEnabled(true);
					mMap.setOnMapClickListener(this);
					mMap.setOnInfoWindowClickListener(this);
				}
			}

		}
		
		getLoaderManager().restartLoader(MAP_ID, null, this);
		
	}
	
//	public void animateToLocation(double lat, double lon) {
//		LatLng latLng = new LatLng(lat,lon);
//		if (mMap != null) {
//			Log.v(TAG,"animating Camera to new position");
//			mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//		}
//	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle b) {
                return new CursorLoader(
                		this.getActivity(), 
                		PlacesContentProvider.GEOPOINT_URI, 
                		ContentProviderColumnsHelper.columnsGeopoint, 
                		null, 
                		null,
                		PlacesContentProvider.GEOPOINT_UPDATETIME_COLUMN+" DESC"
                		);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor c) {
		mMap.clear();
		if (c.getCount()>0) {
			c.moveToFirst();
			double minLat = 999, minLon = 999, maxLat = -999, maxLon = -999; // to find bounds properly
			while (!c.isAfterLast()) {
				double lat = c.getDouble(c.getColumnIndexOrThrow(PlacesContentProvider.GEOPOINT_LAT_COLUMN));
				double lon = c.getDouble(c.getColumnIndexOrThrow(PlacesContentProvider.GEOPOINT_LON_COLUMN));
				//southwest bound
				if (lat<minLat) {minLat=lat;}
				if (lon<minLon) {minLon=lon;}
				if (lat>maxLat) {maxLat=lat;}
				if (lon>maxLon) {maxLon=lon;}
				addPin(
						lat,lon, 
						c.getString(c.getColumnIndexOrThrow(PlacesContentProvider.GEOPOINT_NAME)),
						c.getString(c.getColumnIndexOrThrow(PlacesContentProvider.GEOPOINT_VICINITY))
						);
				c.moveToNext();
			}
			LatLng sw = new LatLng(minLat,minLon);
			LatLng ne = new LatLng(maxLat,maxLon);
			LatLngBounds bounds = new LatLngBounds(sw,ne);
			WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int width = size.x;
			int height = size.y-100;
			Log.v(TAG,"w:"+width+"h:"+height);
			try {
				mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,width,height,200));
			} catch (java.lang.IllegalStateException e)
			{
				Log.e(TAG,e.toString());
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> l) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void onMapClick(LatLng l) {
		MainActivity a = (MainActivity) getActivity();
		a.fillData(l);		
	}
	
	private void launchGoogleMaps(String name, String vicinity) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
			Uri.parse("http://maps.google.com/maps?q="+name+","+vicinity));
			intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
			startActivity(intent);
	}

	@Override
	public void onInfoWindowClick(Marker m) {
		launchGoogleMaps(m.getTitle(), m.getSnippet());
	}
	
}
