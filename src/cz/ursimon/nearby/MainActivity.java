package cz.ursimon.nearby;

import java.util.ArrayList;
import java.util.Locale;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.IntentSender;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import cz.ursimon.nearby.R;
import cz.ursimon.nearby.app.MyVolley;
import cz.ursimon.nearby.data.Constants;
import cz.ursimon.nearby.data.PlacesResponse;
import cz.ursimon.nearby.data.ResultItem;
import cz.ursimon.nearby.fragment.NearbyListFragment;
import cz.ursimon.nearby.fragment.NearbyMapFragment;
import cz.ursimon.nearby.provider.PlacesContentProvider;
import cz.ursimon.nearby.provider.PlacesContentProviderClient;
import cz.ursimon.nearby.util.GsonRequest;

public class MainActivity 
extends 
	FragmentActivity 
implements 
	ActionBar.TabListener,
    GooglePlayServicesClient.ConnectionCallbacks,
    GooglePlayServicesClient.OnConnectionFailedListener, 
    OnItemClickListener, 
    LocationListener
	{

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	private SectionsPagerAdapter mSectionsPagerAdapter;
	
	// for save instance
	private static final String STATE_SELECTED_VIEWPAGER_ITEM = "state_viewpager_position";
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;
	
	//drawer related
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	//location client
	private LocationClient mLocationClient;
	
	private int selectedIndex = 78; // default selected type = restaurant
	
	// Define a request code to send to Google Play services. This code is returned in Activity.onActivityResult
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    
    private final String TAG = getClass().getSimpleName().toString(); // for log

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		setupNavigationDrawer();
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
		}
		
		mLocationClient = new LocationClient(this, this, this);		
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case R.id.action_clear:
			PlacesContentProviderClient.removeAllGeoPoint(this);
			return true;
		case R.id.action_refresh: // will force refresh
			getLocation();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_VIEWPAGER_ITEM)) {				
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_VIEWPAGER_ITEM));
		}
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {				
			selectedIndex = savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_VIEWPAGER_ITEM, getActionBar().getSelectedNavigationIndex());
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, selectedIndex);
	}
	
	/**
	 * 
	 */
	private void setupNavigationDrawer() {
		// Setup navigation drawer
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerListView = (ListView) findViewById(R.id.sidemenu_drawer_listview);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter
				.createFromResource(this, 
						R.array.google_places_types, 
						android.R.layout.simple_list_item_1);
		
		mDrawerListView.setAdapter(adapter);

		// Set the list's click listener
		mDrawerListView.setOnItemClickListener(this);

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
				supportInvalidateOptionsMenu(); // creates call to
												// onPrepareOptionsMenu()
				
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
				supportInvalidateOptionsMenu(); // creates call to
												// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}
	
    /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
        // Connect the client.
        mLocationClient.connect();
    }

    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
    
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPostCreate(android.os.Bundle)
	 */
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			FragmentManager fm = getSupportFragmentManager();
			Fragment result = null;
			switch (position) {
			case 0: // list
				result = fm.findFragmentByTag(NearbyListFragment.TAG_FRAGMENT_LIST);
				if (result==null) {
					result = new NearbyListFragment();
				}
				break;
			case 1: // map
				result = fm.findFragmentByTag(NearbyMapFragment.TAG_FRAGMENT_MAP);
				if (result==null) {
					result = new NearbyMapFragment();
				}		
				break;
			}
			return result;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			}
			return null;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a toast to the
             * user with the error.
             */
        	Toast.makeText(this, "Error:"+connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
        }

	}
	
	public void fillData(LatLng location) {
		Location l = new Location("clicked");
		l.setLatitude(location.latitude);
		l.setLongitude(location.longitude);
		fillData(l);
	}
	
	private void fillData(Location location) {
		setProgressBarIndeterminateVisibility(true);
		Log.v(TAG,"selected index:"+selectedIndex);
		String selectedType = getResources().getStringArray(R.array.google_places_types)[selectedIndex];
		setTitle(getResources().getString(R.string.app_name)+" "+selectedType);
		String callURL = String.format(Locale.US,
							Constants.PLACES_URL_TEMPLATE,
							selectedType,
							location.getLatitude(),
							location.getLongitude(),
							getResources().getString(R.string.api_key_maps_web) // this needs to be API key for web apps
							);    
		Log.v(TAG, "url:" + callURL);
		RequestQueue queue = MyVolley.getRequestQueue();
		GsonRequest<PlacesResponse> categoryRequest = new GsonRequest<PlacesResponse>(Method.GET, callURL,
				PlacesResponse.class, createMyReqSuccessListener(), createMyReqErrorListener());
		queue.add(categoryRequest);
		// Log.v(TAG,"GSON request added to queue, cached:"+categoryRequest.shouldCache());
	}
	
	private Response.Listener<PlacesResponse> createMyReqSuccessListener() {
		return new Response.Listener<PlacesResponse>() {
			@SuppressWarnings("unchecked")
			@Override
			public void onResponse(PlacesResponse response) {

				Log.v(TAG,"response status:"+response.status);
				// Toast.makeText(MainActivity.this, "response: "+response.status, Toast.LENGTH_SHORT).show();
				setProgressBarIndeterminateVisibility(false);

				if (response.status.equalsIgnoreCase("OK")) {

					if (response.results!=null) {
						Log.v(TAG,"response count:"+response.results.size());
						
						if (response.results.size()>0) {
							new SaveResultsTask().execute((ArrayList<ResultItem>)response.results);
						}
						
					}

				}

			}
		};
	}

	private Response.ErrorListener createMyReqErrorListener() {
		return new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				setProgressBarIndeterminateVisibility(false);
				Toast.makeText(MainActivity.this, "error: "+error.getMessage(), Toast.LENGTH_SHORT).show();
				
			}
		};
	}
	
	private void gotLocation(Location whatLocation) {
        Toast.makeText(this, "Lat:"+whatLocation.getLatitude()+" Lon:"+whatLocation.getLongitude(), Toast.LENGTH_SHORT).show();
        fillData(whatLocation);
        Log.v(TAG,"Latitude:"+whatLocation.getLatitude()+" Longitude:"+whatLocation.getLongitude());
	}
	
	@Override
	public void onLocationChanged(Location l) {
		mLocationClient.removeLocationUpdates(this); // disable listener	
		gotLocation(l);
	}
	
	private void getLocation() {
        Location mCurrentLocation = mLocationClient.getLastLocation();
        
        if (mCurrentLocation!=null) {
        	gotLocation(mCurrentLocation);
        } else
        {
        	LocationRequest locationRequest = LocationRequest.create();
	        locationRequest.setInterval(1000);
	        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
			mLocationClient.requestLocationUpdates(locationRequest, this);
        }
	}

	@Override 
	public void onConnected(Bundle b) {   
		// Display the connection status
        //Toast.makeText(this, "Google Services Connected", Toast.LENGTH_SHORT).show();
		getLocation();        
	}

	@Override
	public void onDisconnected() {
		// Display the connection status
        Toast.makeText(this, "Google Services Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
	}
	
	private class SaveResultsTask extends AsyncTask<ArrayList<ResultItem>, Void, Void> {

		@Override
		protected void onPreExecute() {
			PlacesContentProvider.setNotificationsSuspended(MainActivity.this, true); // suspend notifications
			PlacesContentProviderClient.removeAllGeoPoint(MainActivity.this);
		}

		@Override
		protected Void doInBackground(ArrayList<ResultItem>... items) {
			ArrayList<ResultItem> itemsToSave = items[0];
			for (int i=0;i<itemsToSave.size();i++) { 
				ResultItem item = itemsToSave.get(i);
				Log.v(TAG,"saving "+item.name+" "+item.geometry.location.lat+" "+item.geometry.location.lng);
				
				String thumb = "";
				if (item.photos!=null) { // if exists
					if (item.photos.size()>0) { // and contains at least 1 item
						thumb = item.photos.get(0).photo_reference;
					}
				}
				
				PlacesContentProviderClient.addGeoPoint( // lat,lon, placeId, icon, name, rating, ref, vicinity, 
						item.geometry.location.lat,
						item.geometry.location.lng,
						item.id,
						item.icon,
						thumb,
						item.name,
						item.rating,
						item.reference,
						item.vicinity,
						MainActivity.this);
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.v(TAG,"results saved");
			PlacesContentProvider.setNotificationsSuspended(MainActivity.this, false); // fire up all notifications
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int p, long arg3) {
		Log.v(TAG,"onItemClick:"+p);
		mDrawerLayout.closeDrawers();
		selectedIndex = p;
		PlacesContentProviderClient.removeAllGeoPoint(this);
		getLocation();
	}

}
