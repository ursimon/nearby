package cz.ursimon.nearby.data;

public class Constants {

	public final static String PLACES_URL_TEMPLATE =  
			"https://maps.googleapis.com/maps/api/place/search/json?types=%s&location=%f,%f&sensor=true&key=%s&rankby=distance";
	
	public final static String PLACES_IMAGE_URL_TEMPLATE = 
			"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=%s&sensor=true&key=%s";
	
}
