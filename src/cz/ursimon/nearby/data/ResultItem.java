package cz.ursimon.nearby.data;

import java.util.List;

public class ResultItem {

	public Geometry geometry;
	public String icon;
	public String id;
	public String name;
	public List<PhotoItem> photos;
	public double rating;
	public String reference;
	public String[] types;
	public String vicinity;
	
}
