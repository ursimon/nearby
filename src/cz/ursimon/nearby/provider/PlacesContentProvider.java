/**********************************************************************************************************************************************************************
****** AUTO GENERATED FILE BY ANDROID SQLITE HELPER SCRIPT BY FEDERICO PAOLINELLI. ANY CHANGE WILL BE WIPED OUT IF THE SCRIPT IS PROCESSED AGAIN. *******
**********************************************************************************************************************************************************************/
package cz.ursimon.nearby.provider;


import java.util.HashSet;
import java.util.LinkedList;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class PlacesContentProvider extends ContentProvider {
    private static final String DATABASE_NAME = "nearby.db";
    private static final int DATABASE_VERSION = 4;
    private static final String TAG = "PlacesContentProvider";


    // -------------- URIS ------------

    public static final Uri GEOPOINT_URI = Uri.parse("content://cz.ursimon.nearby.provider.PlacesContentProvider/geopoint");


    public static final String ROW_ID = "_id";
    
    // -------------- GEOPOINT DEFINITIONS ------------

    public static final String GEOPOINT_TABLE = "GeoPoint";
    public static final String GEOPOINT_LAT_COLUMN = "Lat";
    public static final String GEOPOINT_LON_COLUMN = "Lon";
    public static final String GEOPOINT_ID = "PlaceId";
    public static final String GEOPOINT_ICON = "Icon";
    public static final String GEOPOINT_THUMB = "Thumb";
    public static final String GEOPOINT_NAME = "Name";
    public static final String GEOPOINT_RATING = "Rating";
    public static final String GEOPOINT_REFERENCE = "Ref";
    public static final String GEOPOINT_VICINITY = "Vicinity";
    public static final String GEOPOINT_UPDATETIME_COLUMN = "UpdateTime";

    private static final int ALLGEOPOINT= 3;
    private static final int SINGLE_GEOPOINT= 4;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("cz.ursimon.nearby.provider.PlacesContentProvider", "geopoint", ALLGEOPOINT);
        uriMatcher.addURI("cz.ursimon.nearby.provider.PlacesContentProvider", "geopoint/#", SINGLE_GEOPOINT);
    }

    // -------- TABLES CREATION ----------

    // GeoPoint CREATION 
    private static final String DATABASE_GEOPOINT_CREATE = "create table " + GEOPOINT_TABLE + " (" + 
				 ROW_ID + " integer primary key autoincrement" + ", " + 
				 GEOPOINT_LAT_COLUMN + " real, " + 
				 GEOPOINT_LON_COLUMN + " real, " + 
				 GEOPOINT_ID + " text unique, " + 
				 GEOPOINT_ICON + " text, " +
				 GEOPOINT_THUMB + " text, " +
				 GEOPOINT_NAME + " text, " +
				 GEOPOINT_RATING + " real, " +
				 GEOPOINT_REFERENCE + " text, " +
				 GEOPOINT_VICINITY + " text, " +
				 GEOPOINT_UPDATETIME_COLUMN + " integer);";

    private MyDbHelper myOpenHelper;

    @Override
    public boolean onCreate() {
        myOpenHelper = new MyDbHelper(getContext(), DATABASE_NAME, null, DATABASE_VERSION);
        return true;
    }

    /**
    * Returns the right table name for the given uri
    * @param uri
    * @return
    */
    private String getTableNameFromUri(Uri uri){
        switch (uriMatcher.match(uri)) {
            case ALLGEOPOINT:
            case SINGLE_GEOPOINT:
                return GEOPOINT_TABLE;
            default: break;
        }

           return null;
    }

	/**
    * Returns the parent uri for the given uri
    * @param uri
    * @return
    */
    private Uri getContentUriFromUri(Uri uri){
        switch (uriMatcher.match(uri)) {
            case ALLGEOPOINT:
            case SINGLE_GEOPOINT:
                return GEOPOINT_URI;
            default: break;
        }

        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
        String[] selectionArgs, String sortOrder) {

        // Open thedatabase.
        SQLiteDatabase db;
        try {
            db = myOpenHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = myOpenHelper.getReadableDatabase();
        }

        // Replace these with valid SQL statements if necessary.
        String groupBy = null;
        String having = null;

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // If this is a row query, limit the result set to the passed in row.
        switch (uriMatcher.match(uri)) {
            case SINGLE_GEOPOINT:
                String rowID = uri.getPathSegments().get(1);
                queryBuilder.appendWhere(ROW_ID + "=" + rowID);
            default: break;
        }

        // Specify the table on which to perform the query. This can
        // be a specific table or a join as required.
        queryBuilder.setTables(getTableNameFromUri(uri));

        // Execute the query.
        Cursor cursor = queryBuilder.query(db, projection, selection,
                    selectionArgs, groupBy, having, sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);

        // Return the result Cursor.
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        // Return a string that identifies the MIME type
        // for a Content Provider URI
        switch (uriMatcher.match(uri)) {
            case ALLGEOPOINT:
                return "vnd.android.cursor.dir/vnd.com.mercedes.fleetapp.geopoint";
            case SINGLE_GEOPOINT:
                return "vnd.android.cursor.dir/vnd.com.mercedes.fleetapp.geopoint";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
            }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = myOpenHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case SINGLE_GEOPOINT:
                String rowID = uri.getPathSegments().get(1);
                selection = ROW_ID + "=" + rowID + (!TextUtils.isEmpty(selection) ?  " AND (" + selection + ')' : "");
            default: break;
        }


        if (selection == null)
            selection = "1";

        int deleteCount = db.delete(getTableNameFromUri(uri),
                selection, selectionArgs);

        notifyChange(uri);

        return deleteCount;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = myOpenHelper.getWritableDatabase();
        String nullColumnHack = null;

        //long id = db.insert(getTableNameFromUri(uri), nullColumnHack, values);
        long id = db.insertWithOnConflict(getTableNameFromUri(uri), nullColumnHack, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id > -1) {
            Uri insertedId = ContentUris.withAppendedId(getContentUriFromUri(uri), id);
                                //getContext().getContentResolver().notifyChange(insertedId, null);
            notifyChange(insertedId);
            return insertedId;
        } else {
            return null;
        }
    }
    
    @Override
	public synchronized int bulkInsert(Uri uri, ContentValues[] values) {
		final SQLiteDatabase db = myOpenHelper.getWritableDatabase();
		final int match = uriMatcher.match(uri);
		switch(match){
		case ALLGEOPOINT:
			int numInserted= 0;
			db.beginTransaction();
			try {

				for (int i = 0; i < values.length; i++){
					//bind the 1-indexed ?'s to the values specified
					db.insert(GEOPOINT_TABLE, null, values[i]);
				}
				db.setTransactionSuccessful();
				numInserted = values.length;
			} finally {
				db.endTransaction();
			}
			return numInserted;

		default:
			throw new UnsupportedOperationException("unsupported uri: " + uri);
		}}

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        // Open a read / write database to support the transaction.
        SQLiteDatabase db = myOpenHelper.getWritableDatabase();

        // If this is a row URI, limit the deletion to the specified row.
        switch (uriMatcher.match(uri)) {
            case SINGLE_GEOPOINT:
                String rowID = uri.getPathSegments().get(1);
                selection = ROW_ID + "=" + rowID + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
            default: break;
        }

        // Perform the update.
        int updateCount = db.update(getTableNameFromUri(uri), values, selection, selectionArgs);

        // Notify any observers of the change in the data set.
        notifyChange(uri);

        return updateCount;
    }
    
    private static boolean suspendNotifications = false;
    private static LinkedList<Uri> suspendedNotifications = new LinkedList<Uri>();
    private static HashSet<Uri> suspendedNotificationsSet = new HashSet<Uri>();
    
    private void notifyChange(Uri uri) {
    	if (suspendNotifications) {
    		synchronized (suspendedNotificationsSet) {
    			if (suspendedNotificationsSet.contains(uri)) {
    				suspendedNotifications.remove(uri);
    			}
    			suspendedNotifications.add(uri);
    			suspendedNotificationsSet.add(uri);
    		}
    	} else {
    		getContext().getContentResolver().notifyChange(uri, null);
    	}
    }
    
    public static void setNotificationsSuspended(Context c ,boolean suspended) {
    	suspendNotifications = suspended;
    	if (!suspended) {
    		Uri uri;
			while ((uri = suspendedNotifications.poll()) != null) {
    			c.getContentResolver().notifyChange(uri, null);
    			suspendedNotificationsSet.remove(uri);
    		}
    	}
    }


    private static class MyDbHelper extends SQLiteOpenHelper {
    
        public MyDbHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        // Called when no database exists in disk and the helper class needs
        // to create a new one. 
        @Override
        public void onCreate(SQLiteDatabase db) {      
			db.execSQL(DATABASE_GEOPOINT_CREATE);
			
        }

        // Called when there is a database version mismatch meaning that the version
        // of the database on disk needs to be upgraded to the current version.
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Log the version upgrade.
            Log.w(TAG, "Upgrading from version " + 
                        oldVersion + " to " +
                        newVersion + ", which will destroy all old data");
            
            // Upgrade the existing database to conform to the new version. Multiple 
            // previous versions can be handled by comparing _oldVersion and _newVersion
            // values.

            // The simplest case is to drop the old table and create a new one.
			db.execSQL("DROP TABLE IF EXISTS " + GEOPOINT_TABLE + ";");
			
            // Create a new one.
            onCreate(db);
        }
    }
    
}