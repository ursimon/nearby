package cz.ursimon.nearby.provider;

public class ContentProviderColumnsHelper {
	
	public static String[] columnsGeopoint = new String[] {
        	PlacesContentProvider.ROW_ID,
    		PlacesContentProvider.GEOPOINT_LAT_COLUMN,
    		PlacesContentProvider.GEOPOINT_LON_COLUMN,
    		PlacesContentProvider.GEOPOINT_ID, 
    		PlacesContentProvider.GEOPOINT_ICON,
    		PlacesContentProvider.GEOPOINT_THUMB,
    		PlacesContentProvider.GEOPOINT_NAME,
    		PlacesContentProvider.GEOPOINT_RATING,
    		PlacesContentProvider.GEOPOINT_REFERENCE,
    		PlacesContentProvider.GEOPOINT_VICINITY,
    		PlacesContentProvider.GEOPOINT_UPDATETIME_COLUMN  }; 
	
}
