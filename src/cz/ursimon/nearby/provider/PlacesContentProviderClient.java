/**********************************************************************************************************************************************************************
 ****** AUTO GENERATED FILE BY ANDROID SQLITE HELPER SCRIPT BY FEDERICO PAOLINELLI. ANY CHANGE WILL BE WIPED OUT IF THE SCRIPT IS PROCESSED AGAIN. *******
 **********************************************************************************************************************************************************************/
package cz.ursimon.nearby.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

public class PlacesContentProviderClient {
	@SuppressWarnings("unused")
	private final static String TAG = "PlacesContentProviderClient";

	// -------------- GEOPOINT HELPERS ------------------

	public static Uri addGeoPoint(double lat,double lon,
			String placeId, String icon, String thumb, String name, double rating, String ref, String vicinity,
			Context c) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(PlacesContentProvider.GEOPOINT_LAT_COLUMN,lat);
		contentValues.put(PlacesContentProvider.GEOPOINT_LON_COLUMN,lon);		
		contentValues.put(PlacesContentProvider.GEOPOINT_ID,placeId);
		contentValues.put(PlacesContentProvider.GEOPOINT_ICON,icon);
		contentValues.put(PlacesContentProvider.GEOPOINT_THUMB,thumb);
		contentValues.put(PlacesContentProvider.GEOPOINT_NAME,name);
		contentValues.put(PlacesContentProvider.GEOPOINT_RATING,rating);
		contentValues.put(PlacesContentProvider.GEOPOINT_REFERENCE,ref);
		contentValues.put(PlacesContentProvider.GEOPOINT_VICINITY,vicinity);
		contentValues.put(PlacesContentProvider.GEOPOINT_UPDATETIME_COLUMN,
				System.currentTimeMillis());
		ContentResolver cr = c.getContentResolver();
		return cr.insert(PlacesContentProvider.GEOPOINT_URI, contentValues);
	}

	public static int removeGeoPoint(long rowIndex, Context c) {
		ContentResolver cr = c.getContentResolver();
		Uri rowAddress = ContentUris.withAppendedId(
				PlacesContentProvider.GEOPOINT_URI, rowIndex);
		return cr.delete(rowAddress, null, null);
	}

	public static int removeAllGeoPoint(Context c) {
		ContentResolver cr = c.getContentResolver();
		return cr.delete(PlacesContentProvider.GEOPOINT_URI, null, null);
	}

	public static Cursor getAllGeoPoints(Context c) {
		ContentResolver cr = c.getContentResolver();

		String where = null;
		String whereArgs[] = null;
		String order = null;

		Cursor resultCursor = cr.query(PlacesContentProvider.GEOPOINT_URI,
				ContentProviderColumnsHelper.columnsGeopoint, where, whereArgs,
				order);
		return resultCursor;
	}

	public static Cursor getGeoPoint(long rowId, Context c) {
		ContentResolver cr = c.getContentResolver();

		Uri rowAddress = ContentUris.withAppendedId(
				PlacesContentProvider.GEOPOINT_URI, rowId);

		String where = null;
		String whereArgs[] = null;
		String order = null;

		Cursor resultCursor = cr.query(rowAddress,
				ContentProviderColumnsHelper.columnsGeopoint, where, whereArgs,
				order);
		return resultCursor;
	}

	public static int updateGeoPoint(long rowId, LatLng geoPoint,
			String placeId, String icon, String thumb, String name, float rating, String ref, String vicinity,
			Context c) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(PlacesContentProvider.GEOPOINT_LAT_COLUMN, geoPoint.latitude);
		contentValues.put(PlacesContentProvider.GEOPOINT_LON_COLUMN, geoPoint.longitude);
		
		contentValues.put(PlacesContentProvider.GEOPOINT_ID,placeId);
		contentValues.put(PlacesContentProvider.GEOPOINT_ICON,icon);
		contentValues.put(PlacesContentProvider.GEOPOINT_THUMB,thumb);
		contentValues.put(PlacesContentProvider.GEOPOINT_NAME,name);
		contentValues.put(PlacesContentProvider.GEOPOINT_RATING,rating);
		contentValues.put(PlacesContentProvider.GEOPOINT_REFERENCE,ref);
		contentValues.put(PlacesContentProvider.GEOPOINT_VICINITY,vicinity);
		
		contentValues.put(PlacesContentProvider.GEOPOINT_UPDATETIME_COLUMN,
				System.currentTimeMillis());

		Uri rowURI = ContentUris.withAppendedId(
				PlacesContentProvider.GEOPOINT_URI, rowId);

		String where = null;
		String whereArgs[] = null;

		ContentResolver cr = c.getContentResolver();
		int updatedRowCount = cr
				.update(rowURI, contentValues, where, whereArgs);
		return updatedRowCount;
	}

}